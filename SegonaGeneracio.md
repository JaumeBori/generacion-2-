#  Segona generació

![Texto alternativo](http://2.bp.blogspot.com/-hoB4q0_Xc5o/T8zv4wmB1hI/AAAAAAAAAAY/cA7HCgnO_ak/s320/segunda+generacion.bmp)

## *Introducció*
---
![Texto alternativo](http://1.bp.blogspot.com/-LdScLn1R88o/UV2GhPrYsOI/AAAAAAAADAc/6qqomhndM0w/s400/UNIVAC-1-GraceHopper-762x600.jpg)

##### La segona generació de llenguatges va ser desenvolupada a finals dels anys 50 i principis dels 60 i ha servit com a base per a tots els llenguatges de programació moderns (tercera generació).

## *Versions*
---

1. Fortran: Una potent eina per a la resolució de problemes computacionals. La part negativa és que li faltava el suport directe d'estructures de control.
2. Ansi: Corregeix algunes de les deficiències trobades en versions anteriors del llenguatge.

![Texto alternativo](https://www.informatica-hoy.com.ar/imagenes-notas/generaciones-de-la-computadora-pc-xt.jpg)

## *Versió Definitiva (ALGOL)*

##### Ofereix un repertori extremadament ric de construccions procedimentals i de tipificació de dades. L'assignació dinàmica de memòria, de recursivitat i altres característiques amb gran influència en els llenguatges moderns, són els que han fet destacar aquesta versió a Europa i països d'occident.

![Texto alternativo](http://i1248.photobucket.com/albums/hh495/buho21/LenguajeBASIC.jpg)

# Generacions dels llenguatges de programació
[Tercera-Generació](https://gitlab.com/JaviEV/tercera_generacion/blob/master/tercera_generacion.md)

[Quarta_Generació](https://gitlab.com/53335394g/primera_generacion/blob/master/cuarta_generaci%C3%B3n.md)

[Cinquena-Generació](https://gitlab.com/JaumeBori/generacion-2-/blob/master/5gen.md)