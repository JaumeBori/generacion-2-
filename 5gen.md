#  Cinquena generació

## *Introducció*
---
![Texto alternativo](https://franz.com/images/stepper-dialog-2.png)

##### Esta generación incluye la inteligencia artificial y sistemas expertos.

## *Versions*
---

1. Lisp: John McCarthy el seu fundador volia un llenguatge que mostrés la informació estructurada en llistes en les que es poguessin gestionar aquesta.

```lisp
(format t "¡Hola, mundo!")

```

2. Prolog: Llenguatges de programació basats és estructures de control i definició de funcions per calcular resultats.

3. OPS5: El llenguatge OPS5 és potser el menys conegut dels nomenats. Tanmateix, la seva importància en el desenvolupament de la intel·ligència artificial va ser clau ja que va ser el primer llenguatge usat amb resultat d'èxit.

![Texto alternativo](http://3.bp.blogspot.com/-icgWetNW-8Q/UZLSswaBHgI/AAAAAAAAAFE/mhFt7WijO7Y/s1600/dame.gif)

# Generacions dels llenguatges de programació
[Preguntas](https://gitlab.com/JaumeBori/generacion-2-/blob/master/fi.md)
